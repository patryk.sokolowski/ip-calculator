const ERROR_INCORRECT_MASK_INPUT_VALUE = "Podana maska jest nieprawidłowa.";
const ERROR_INCORRECT_MASK_LIMIT_INPUT_VALUE = "Podany limit maski jest nieprawidłowy.";
const ERROR_NO_RANGES_FOUND = "Wystąpił błąd. Prawdopodobnie musisz najpierw obliczyć zakresy sieci, zanim przystąpisz do tej akcji.";
const STATUS_TEXT_TAKEN = "Zajete";
const STATUS_TEXT_FREE = "Wolne";
// const BUTTON_TEXT_RESERVE = "Rezerwuj";
const BUTTON_TEXT_FIND = "Znajdź";
const INFO_TEXT_TOOLTIP = "W tym polu administrator powinien wpisać limit maski, dla której chce liczyć podsieci.";
const INFO_TEXT_HOSTS_COUNT = "Liczba hostów w danej sieci";
const INFO_TEXT_SEARCH_FRAGMENT_BOTTOM_LIMIT = "Znajdź najmniejszy kawałek, nie mniejszy, niż /";
const INFO_TEXT_CALCULATED_SUBNETS = "Wyliczone podsieci"
const INFO_TEXT_SUBNETS_AFTER_EXCLUDING = "Niezarezerwowane, większe podsieci";
const INFO_TEXT_NO_MATCH_FOUND = "Nie znaleziono wyniku.";
const IP_INPUT_FRAGMENTS_CLASS = "ipInput";
const IP_MASK_INPUT_ID = "Mask";
const IP_MASK_LIMIT_INPUT_ID = "maskLimitInput";
const IP_AND_MASK_MAX_LENGTH = 32;
const BUTTON_ID_HOST = 'Host';
const BUTTON_ID_NETWORK = 'Network';
const BUTTON_ID_FIND_NETWORK = 'FindNetwork';
const BUTTON_ID_TOOLTIP = "tooltip";
const IP_FRAGMENTS_NUMBER = 4;
const view_findNetwork = '<h4 id="text_downFragmentLimit"><input type="input id="maskLimitInput"></h4>'

document.addEventListener("DOMContentLoaded", function() {
	var controller = new Controller();

	var view = document.querySelector('.content');

	var maskInput = document.getElementById(IP_MASK_INPUT_ID);
	var tooltip = document.getElementById(BUTTON_ID_TOOLTIP);
	tooltip.addEventListener('mouseover', function() {
		var buttonBoundingData = this.getBoundingClientRect();
		console.log("Tooltip clicked");

		var tooltipAreaElement = document.createElement("div");
		tooltipAreaElement.setAttribute("id", "tooltipArea");
		document.body.appendChild(tooltipAreaElement);

		// document.body.innerHTML += "<div id='tooltipArea'><button id='tooltip--close' class='btn'>X</button><p>" + INFO_TEXT_TOOLTIP + "</p></div>";
		var tooltipArea = document.getElementById('tooltipArea');
		tooltipArea.innerHTML = "<p>" + INFO_TEXT_TOOLTIP + "</p>";
		tooltipArea.style.position = "absolute";
		tooltipArea.style.left = Math.round(buttonBoundingData.x - (buttonBoundingData.width + 10)) + "px";
		tooltipArea.style.top = Math.round(buttonBoundingData.y + buttonBoundingData.height + 10) + "px";

		console.log("Pos: ", buttonBoundingData);
	});

	tooltip.addEventListener('mouseout', function() {
		tooltipArea.remove();
	});

	var hostButton = document.getElementById(BUTTON_ID_HOST);
	hostButton.addEventListener('click', function() {
		view.innerHTML = "<h4 class='correction__margin--all correction__margin--left25'>" + INFO_TEXT_HOSTS_COUNT + "</h4>";
		view.innerHTML += "<h6 class='correction__margin--all correction__margin--left25'>" + controller.getHosts(IP_INPUT_FRAGMENTS_CLASS, IP_MASK_INPUT_ID) + "</h6>";
	});

	var networkButtonTemp = document.getElementById(BUTTON_ID_NETWORK);
	networkButtonTemp.addEventListener('click', function() {
		controller.cleanRangesArray();

		var hostsRanges = controller.getHostsRanges(IP_INPUT_FRAGMENTS_CLASS, IP_MASK_INPUT_ID, IP_MASK_LIMIT_INPUT_ID);
		console.log("Debug hostsRanges length: " + hostsRanges.length);
		view.innerHTML = "";
		var maskValue = document.getElementById(IP_MASK_INPUT_ID).value;

		if (hostsRanges != ERROR_INCORRECT_MASK_INPUT_VALUE && hostsRanges != ERROR_INCORRECT_MASK_LIMIT_INPUT_VALUE) {
			view.innerHTML = "<h4>" + INFO_TEXT_CALCULATED_SUBNETS + "</h4>";
			if (hostsRanges.length > 0) {
				console.log("hostsRanges.length: " + hostsRanges.length);
				for (let i = 0; i < hostsRanges.length; i++) {
					console.log("[" + i + "]: ", hostsRanges[i]);

					let networkAddress = hostsRanges[i].rangeStart.slice(0);
					networkAddress[networkAddress.length - 1] = parseInt(networkAddress[networkAddress.length - 1], 10) - 1;

					view.innerHTML += "<h6 class='subnet__status status__indicator--free'>" + networkAddress.join(".") + "/" + hostsRanges[i].mask + " | " +
					hostsRanges[i].rangeStart.join(".") + " - " + hostsRanges[i].rangeEnd.join(".") + "<input type='checkbox' class='status__checkbox checkbox" + i +
					"'></h6><h6 class='status__indicator status__indicator--free'>" + STATUS_TEXT_FREE + "</h6></br>";
				}

				view.innerHTML += "<div id='recalculatedRangesContainer'></div>";

				addStatusCheckboxListeners(controller, false);
			} else {
				view.innerHTML += "<h6 class='correction__margin--all correction__margin--left25'>" + INFO_TEXT_NO_MATCH_FOUND + "</h6>";
			}
		} else {
			view.innerHTML = "<h4>" + hostsRanges + "</h4>";
		}
		/*
		 * Below function is executed for debug purposes only.
		 */
		controller.getRange(IP_INPUT_FRAGMENTS_CLASS, IP_MASK_INPUT_ID);
	});

	var findNetworkTemp = document.getElementById(BUTTON_ID_FIND_NETWORK);
	// findNetworkTemp.addEventListener('click', function() {
	// 	// var subnetsRanges = [];
	// 	// subnetsRanges = controller.getSubnetsRanges(IP_INPUT_FRAGMENTS_CLASS, IP_MASK_INPUT_ID, IP_MASK_LIMIT_INPUT_ID);
	// 	// var hostsRanges = controller.getHostsRanges(IP_INPUT_FRAGMENTS_CLASS, IP_MASK_INPUT_ID, IP_MASK_LIMIT_INPUT_ID);
	// 	// var hostsRangesArray = [];
	// 	// view.innerHTML = "";
	// 	// for (var i = 0; i < hostsRanges.length / IP_FRAGMENTS_NUMBER; i++) {
	// 	// 	hostsRangesArray.push(hostsRanges.slice(i * IP_FRAGMENTS_NUMBER, i * IP_FRAGMENTS_NUMBER + IP_FRAGMENTS_NUMBER));
	// 	// }
	// 	// var maskValue = document.getElementById(IP_MASK_INPUT_ID).value;
	// 	// for (var i = 0; i < subnetsRanges.length; i++) {
	// 	// 	var subnetRange1 = subnetsRanges[i].slice(0, 4).join(".");
	// 	// 	var subnetRange2 = subnetsRanges[i].slice(4, 8).join(".");
	// 	// 	// view.innerHTML += subnetRange1 + " - " + subnetRange2;
	// 	// 	view.innerHTML += subnetRange1 + "/" + (parseInt(maskValue) + 1 + i);
	// 	// 	view.innerHTML += " | ";
	// 	// 	view.innerHTML += hostsRangesArray[i * 2].join(".") + " - " + hostsRangesArray[i * 2 + 1].join(".");
	// 	// 	view.innerHTML += "</br>";
	// 	// }
	// 	controller.getRange(IP_INPUT_FRAGMENTS_CLASS, IP_MASK_INPUT_ID);
	// 	view.innerHTML = "Hola mi amigo, no tan rápido!";
	// });

	// var debugListButton = document.getElementById("button_debugList");
	// debugListButton.addEventListener('click', function() {
	// 	console.log("Debug button clicked");
	// 	controller.debug();
	// });
});

function addStatusCheckboxListeners(controller, accessOnlyRangeFinderEvent) {
	function displayRecalculatedRanges(container, recalculatedSubnetsRanges) {
		function addSubnetSearchButtonListener(controller, accessOnlyRangeFinderEvent) {
			document.getElementById('findAddressButton').addEventListener('click', function() {
				var resultDisplayArea = document.getElementById('searchResultDisplayArea');
				var searchMaskField = document.getElementById('searchMask');
				console.log("CLICK");
				var rangesArray = controller.getRecalculatedSubnets();
				// for (let i = rangesArray.length - 1; i >= 0; i--) {
				// 	console.log("Mask " + rangesArray[i].mask + " | Field value: " + searchMaskField.value);
				// 	if (rangesArray[i].mask <= searchMaskField.value) {
				// 		resultDisplayArea.innerHTML = "<h6 class='correction__margin--all correction__margin--left25'>" + rangesArray[i].rangeStart.join(".") + " - " + rangesArray[i].rangeEnd.join(".") + "/" + rangesArray[i].mask + "</h6>";
				// 		console.log("Match found! " + rangesArray[i].rangeStart.join(".") + " - " + rangesArray[i].rangeEnd.join(".") + "/" + rangesArray[i].mask);
				// 		addStatusCheckboxListeners(controller, true);
				// 		break;
				// 	} else if (i-1 < 0) {
				// 		resultDisplayArea.innerHTML = "<h6 class='correction__margin--all correction__margin--left25'>" + INFO_TEXT_NO_MATCH_FOUND + "</h6>";
				// 	}
				// }
				/*
				* Below code is an improvement on the one above, which fullfits the requirement stated by teacher
				* Uncomment when going back to coding
				*/
				for (let i = rangesArray.length - 1; i > 0; i--) {
					console.log("Mask " + rangesArray[i].mask + " | Field value: " + searchMaskField.value);
					if (rangesArray[i].mask >= searchMaskField.value && rangesArray[i-1].mask < searchMaskField.value) {
						resultDisplayArea.innerHTML = "<h6 class='correction__margin--all correction__margin--left25'>" + rangesArray[i].rangeStart.join(".") + " - " + rangesArray[i].rangeEnd.join(".") + "/" + rangesArray[i].mask + "</h6>";
						console.log("Match found! " + rangesArray[i].rangeStart.join(".") + " - " + rangesArray[i].rangeEnd.join(".") + "/" + rangesArray[i].mask);
						addStatusCheckboxListeners(controller, true);
						console.log("First condition");
						break;
					} else if (i - 1 == 0 && rangesArray[i-1].mask >= searchMaskField.value) {
						resultDisplayArea.innerHTML = "<h6 class='correction__margin--all correction__margin--left25'>" + rangesArray[i-1].rangeStart.join(".") + " - " + rangesArray[i-1].rangeEnd.join(".") + "/" + rangesArray[i-1].mask + "</h6>";
						console.log("Match found! " + rangesArray[i-1].rangeStart.join(".") + " - " + rangesArray[i-1].rangeEnd.join(".") + "/" + rangesArray[i-1].mask);
						addStatusCheckboxListeners(controller, true);
						console.log("Second condition");
					} else if (i-2 < 0) {
						resultDisplayArea.innerHTML = "<h6 class='correction__margin--all correction__margin--left25'>" + INFO_TEXT_NO_MATCH_FOUND + "</h6>";
					}
				}
			});
		}

		if (accessOnlyRangeFinderEvent) {
			console.log("accessOnlyRangeFinderEvent: " + true);
			addSubnetSearchButtonListener(controller, accessOnlyRangeFinderEvent);
		} else {
			console.log("accessOnlyRangeFinderEvent: " + false);
			container.innerHTML = "<h4>" + INFO_TEXT_SUBNETS_AFTER_EXCLUDING + "</h4>";
			for (var j = 0; j < recalculatedSubnetsRanges.length; j++) {
				container.innerHTML += "<h6 class='correction__margin--all correction__margin--left25'>" + recalculatedSubnetsRanges[j].rangeStart.join(".") + " - " + recalculatedSubnetsRanges[j].rangeEnd.join(".") + "/" + recalculatedSubnetsRanges[j].mask + "</h6>";
			}

			/*
			 * Test part below
			 * Needs further work - it is currently not working!
			 */
			container.innerHTML += "<h4 class='inlineBlock correction__margin--right'>" + INFO_TEXT_SEARCH_FRAGMENT_BOTTOM_LIMIT + "</h4><input type='input' id='searchMask' class='maskInput' maxlength='2'></br>";
			container.innerHTML += "<button id='findAddressButton' type='button' class='btn btn-lg  btn-success'>" + BUTTON_TEXT_FIND + "</button>";
			container.innerHTML += "<div id='searchResultDisplayArea'></div>";
			// document.getElementById('findAddressButton').addEventListener('click', function() {
			// 	var resultDisplayArea = document.getElementById('recalculatedRangesContainer');
			// 	var searchMaskField = document.getElementById('searchMask');
			// 	console.log("CLICK");
			// 	var rangesArray = controller.getRecalculatedSubnets();
			// 	for (let i = rangesArray.length - 1; i >= 0; i--) {
			// 		console.log("Mask " + rangesArray[i].mask + " | Field value: " + searchMaskField.value);
			// 		if (rangesArray[i].mask <= searchMaskField.value) {
			// 			resultDisplayArea.innerHTML += rangesArray[i].rangeStart.join(".") + " - " + rangesArray[i].rangeEnd.join(".") + "/" + rangesArray[i].mask + "</br>";
			// 			console.log("Match found! " + rangesArray[i].rangeStart.join(".") + " - " + rangesArray[i].rangeEnd.join(".") + "/" + rangesArray[i].mask);
			// 			addStatusCheckboxListeners(controller, true);
			// 			break;
			// 		}
			// 	}
			// 	// console.log("lowest: " + lowest);
			// });
			addSubnetSearchButtonListener(controller, accessOnlyRangeFinderEvent);
		}
	}
	var checkboxArray = document.getElementsByClassName('status__checkbox');
	var takenSubnetsArray = controller.takenSubnetsArray;
	var recalculatedRangesFieldContainer = document.getElementById('recalculatedRangesContainer');

	if (accessOnlyRangeFinderEvent) {
		console.log("accessOnlyRangeFinderEvent: " + true);
		displayRecalculatedRanges(recalculatedRangesFieldContainer, controller.getRecalculatedSubnets());
	}
	else {
		console.log("accessOnlyRangeFinderEvent: " + false);
		for (var i = 0; i < checkboxArray.length; i++) {
			var correlatedStatusText = document.getElementsByClassName('status__indicator');
			var correlatedSubnetRange = document.getElementsByClassName('subnet__status');
			checkboxArray[i].addEventListener('change', function() {
				var index = this.classList[1].slice(this.classList[1].length - 1, this.classList[1].length);
				var currentStatusText = correlatedStatusText[index];
				var currentSubnetRange = correlatedSubnetRange[index];
				if (this.checked) {
					if (currentStatusText.classList.contains('status__indicator--free')) {
						currentStatusText.classList.remove('status__indicator--free');
						currentSubnetRange.classList.remove('status__indicator--free');
					}
					currentStatusText.classList.add('status__indicator--taken');
					currentSubnetRange.classList.add('status__indicator--taken');
					currentStatusText.innerHTML = STATUS_TEXT_TAKEN;

					var indexInArray = takenSubnetsArray.indexOf(index);
					// console.log("indexInArray: " + indexInArray);
					// console.log("takenSubnetsArray[" + indexInArray + "]: " + takenSubnetsArray[indexInArray]);
					if (indexInArray < 0) {
						takenSubnetsArray[takenSubnetsArray.length] = index;
					}

					// displayRecalculatedRanges(recalculatedRangesFieldContainer, controller.getRecalculatedSubnets());
				} else {
					if (currentStatusText.classList.contains('status__indicator--taken')) {
						currentStatusText.classList.remove('status__indicator--taken');
						currentSubnetRange.classList.remove('status__indicator--taken');
					}
					currentStatusText.classList.add('status__indicator--free');
					currentSubnetRange.classList.add('status__indicator--free');
					currentStatusText.innerHTML = STATUS_TEXT_FREE;

					var indexInArray = takenSubnetsArray.indexOf(index);
					if (indexInArray > -1) {
						takenSubnetsArray.splice(indexInArray, 1);
					}

					// displayRecalculatedRanges(recalculatedRangesFieldContainer, controller.getRecalculatedSubnets());
				}
				displayRecalculatedRanges(recalculatedRangesFieldContainer, controller.getRecalculatedSubnets());
				// controller.getRecalculatedSubnets();
			});
		}
		displayRecalculatedRanges(recalculatedRangesFieldContainer, controller.getRecalculatedSubnets());
	}
}

var IPcalculator = function() {
	this.data = {};
	this.ip = {};
	this.mask = {};
	this.subnetsRangesObjectsArray = [];
	this.range = function(ipInputsValues, maskInputValue) {
		/*
		 * Calculate IP ranges for masks from the first passed in IP
		 * and the mask limit passed in mask limit field.
		*/
		var ip = this.calculateIPAddress(ipInputsValues);
		var mask = this.calculateIPMask(maskInputValue);
		var hostAddress = [];
		var broadcastAddress = [];
		var hostAddressString = "";
		var broadcastAddressString = "";
		var range = [];

		for (let i = 0; i < IP_AND_MASK_MAX_LENGTH; i++) {
			if (ip[i] == mask[i] && ip[i] == 1 && mask[i] == 1) {
				hostAddress.push(1);
				hostAddressString += "1";
			}
			else {
				hostAddress.push(0);
				hostAddressString += "0";
			}

			if (i != 0 && (i + 1) % 8 == 0) {
				// console.log("Network address: " + hostAddressString);
				range.push(parseInt(hostAddressString, 2));
				hostAddressString = "";
			}
		}

		for (let i = 0; i < IP_AND_MASK_MAX_LENGTH; i++) {
			if (hostAddress[i] == mask[i] && hostAddress[i] == 0 && mask[i] == 0) {
				broadcastAddress.push(1);
				broadcastAddressString += "1";
			}
			else {
				broadcastAddress.push(hostAddress[i]);
				broadcastAddressString += hostAddress[i].toString();
			}

			if (i != 0 && (i + 1) % 8 == 0) {
				// console.log("Broadcast address: " + broadcastAddressString);
				range.push(parseInt(broadcastAddressString, 2));
				broadcastAddressString = "";
			}
		}
		return range;
	}
	this.recalculateRange = function() {
		/*
		 * After user disables particular IP range,
		 * recalculate new possible IP fragments
		 * that are possible to join into bigger ones.
		*/
	}
	this.compareArrayValues = function(array1, array2) {
		/*
		 * Compare both arrays values from every index and
		 * return true if array1 has lower value than array2
		*/
		var arraySize = Math.min(array1.length, array2.length);
		if (array1.length == array2.length) {
			for (let i = 0; i < arraySize; i++) {
				// console.log("Array1[" + i + "]: " + array1[i] + " || Array2[" + i + "]: " + array2[i]);
				// console.log(array1[i] < array2[i]);
				if (array1[i] < array2[i])
					return true;
				if (array1[i] > array2[i])
					return false;
			}
		}
		return false;
	}
	this.calculateSubnetsRanges = function(ipInputsValues, maskInputValue, maskLimitInputValue) {
		/*
		 * Calculate subnets IP ranges based on IP mask and mask limit.
		 * IP ranges are fetched for every mask starting from
		 * base mask (maskInputValue) to last mask (maskLimitInputValue).
		*/
		var fullRange = this.range(ipInputsValues, maskInputValue);
		var lowestAddress = fullRange.slice(0,4);
		var highestAddress = fullRange.slice(4,8);
		var currentAddressStep = lowestAddress;
		// console.log("First currentAddressStep: " + currentAddressStep);
		var maskSteps = Math.abs(maskInputValue - maskLimitInputValue);
		var subnetsRanges = [];
		if (parseInt(maskLimitInputValue) > parseInt(maskInputValue)) {
			console.log("First condition passed.");
			var loopCounter = 0;
			for (var i = 0; i <= maskSteps && this.compareArrayValues(currentAddressStep, highestAddress); i++, loopCounter++) {
				// console.log(this.compareArrayValues(currentAddressStep, highestAddress));
				if (i >= maskSteps) {
					subnetsRanges[i] = this.range(currentAddressStep, parseInt(maskInputValue, 10) + (i));



					/*
					 * Test functionality - calculated ranges (start range, end range, mask)
					 * are now stored in object's array
					*/
					let currentRangeObject = {};
					currentRangeObject.mask = parseInt(maskInputValue, 10) + (i);
					currentRangeObject.rangeStart = subnetsRanges[i].slice(0,4);
					currentRangeObject.rangeEnd = subnetsRanges[i].slice(4,8);
					console.log("currentRangeObject: " + currentRangeObject.rangeStart + " - " + currentRangeObject.rangeEnd);
					this.subnetsRangesObjectsArray.push(currentRangeObject);
				} else {
					subnetsRanges[i] = this.range(currentAddressStep, parseInt(maskInputValue, 10) + (i+1));
					// console.log("Mask input value in loop: ");
					// console.log(parseInt(maskInputValue, 10) + (i+1));
					// console.log("subnetsRanges: ");
					// console.log(subnetsRanges[i]);
					var tempAddressStep = [];

					// tempAddressStep[0] = subnetsRanges[i][4];
					// tempAddressStep[1] = subnetsRanges[i][5];
					// tempAddressStep[2] = parseInt(subnetsRanges[i][6], 10) + 1;
					// tempAddressStep[3] = 0;



					if (subnetsRanges[i][5] >= 255)
						tempAddressStep[0] = parseInt(subnetsRanges[i][4], 10) + 1;
					else
						tempAddressStep[0] = subnetsRanges[i][4];
					if (subnetsRanges[i][6] >= 255 && subnetsRanges[i][7] >= 255)
						tempAddressStep[1] = parseInt(subnetsRanges[i][5], 10) + 1;
					else
						tempAddressStep[1] = subnetsRanges[i][5];
					tempAddressStep[2] = parseInt(subnetsRanges[i][6], 10) + 1;
					tempAddressStep[3] = 0;
						



					currentAddressStep = tempAddressStep;
					// console.log("currentAddressStep: " + currentAddressStep);
					// console.log("highestAddress: " + highestAddress);



					/*
					 * Test functionality - calculated ranges (start range, end range, mask)
					 * are now stored in object's array
					*/
					let currentRangeObject = {};
					currentRangeObject.mask = parseInt(maskInputValue, 10) + (i+1);
					currentRangeObject.rangeStart = subnetsRanges[i].slice(0,4);
					currentRangeObject.rangeEnd = subnetsRanges[i].slice(4,8);
					console.log("currentRangeObject: " + currentRangeObject.rangeStart + " - " + currentRangeObject.rangeEnd);
					this.subnetsRangesObjectsArray.push(currentRangeObject);
				}
			}
			if (this.compareArrayValues(highestAddress, currentAddressStep)) {
				subnetsRanges[loopCounter-1] = this.range(subnetsRanges[loopCounter-1], parseInt(maskInputValue, 10) + (loopCounter-1));
				// console.log("Test: " + subnetsRanges[loopCounter-1]);


				/*
				 * Test functionality - calculated ranges (start range, end range, mask)
				 * are now stored in object's array
				*/
				let currentRangeObject = {};
				currentRangeObject.mask = parseInt(maskInputValue, 10) + (loopCounter-1);
				currentRangeObject.rangeStart = subnetsRanges[loopCounter-1].slice(0,4);
				currentRangeObject.rangeEnd = subnetsRanges[loopCounter-1].slice(4,8);
				this.subnetsRangesObjectsArray.splice(-1,1);
				console.log("currentRangeObject: " + currentRangeObject.rangeStart + " - " + currentRangeObject.rangeEnd);
				this.subnetsRangesObjectsArray.push(currentRangeObject);
			}
		} else {
			subnetsRanges = 0;
		}
		// console.log("subnetsRanges: " + subnetsRanges);
		// return subnetsRanges;
		return this.subnetsRangesObjectsArray;
	}
	this.recalculateSubnetsAfterExcludingRanges = function(takenSubnetsArray) {
		// console.log("At start: " + takenSubnetsArray);
		if (this.subnetsRangesObjectsArray.length <= 0)
			return ERROR_NO_RANGES_FOUND;
		else {
			var recalculatedSubnetsRanges = [];
			var ranges = [];
			var rangesSingleObject = {};
			rangesSingleObject.mask = null;
			rangesSingleObject.rangeStart = null;
			rangesSingleObject.rangeEnd = null;

			for (var i = 0; i < this.subnetsRangesObjectsArray.length; i++) {
				// console.log("Index check: " + takenSubnetsArray.indexOf(i));
				if (takenSubnetsArray.indexOf(i.toString()) < 0) {
					// console.log("WTF: ", this.subnetsRangesObjectsArray[i]);

					if (rangesSingleObject.mask == null && rangesSingleObject.rangeStart == null) {
						rangesSingleObject.mask = this.subnetsRangesObjectsArray[i].mask;
						rangesSingleObject.rangeStart = this.subnetsRangesObjectsArray[i].rangeStart;
					}

					rangesSingleObject.rangeEnd = this.subnetsRangesObjectsArray[i].rangeEnd;

					if (i + 1 >= this.subnetsRangesObjectsArray.length)
						ranges.push(rangesSingleObject);

					recalculatedSubnetsRanges.push(this.subnetsRangesObjectsArray[i]);
				} else {
					if (rangesSingleObject.rangeStart != null)
						ranges.push(rangesSingleObject);
					rangesSingleObject = {};
					if (i + 1 < this.subnetsRangesObjectsArray.length && takenSubnetsArray.indexOf((i+1).toString()) < 0) {
						rangesSingleObject.mask = this.subnetsRangesObjectsArray[i+1].mask;
						rangesSingleObject.rangeStart = this.subnetsRangesObjectsArray[i+1].rangeStart;
					}
				}
			}
			console.log("recalculatedSubnetsRanges (" + recalculatedSubnetsRanges.length + "): " + recalculatedSubnetsRanges);
			for (let j = 0; j < ranges.length; j++) {
				console.log("ranges[" + j + "]: " + ranges[j].rangeStart + " - " + ranges[j].rangeEnd + "/" + ranges[j].mask);
			}
			/*
			 * In case to get every subnet range listed, use 'return recalculatedSubnetsRanges',
			 * else if only big ranges are needed, use 'return ranges'
			 * Notice: One of below return instructions has to be commented out.
			 */
			// return recalculatedSubnetsRanges;
			return ranges;
		}
	}
	this.excludeNetworkAndBroadcastAddresses =  function(ipRangesObject) {
		var hostsRanges = [];
		for (var i = 0; i < ipRangesObject.length; i++) {
			for (var j = 0; j < ipRangesObject[i].length; j++) {
				if (j == 3) {
					hostsRanges.push(ipRangesObject[i][j] + 1);
				}
				else if (j == 7) {
					hostsRanges.push(ipRangesObject[i][j] - 1);
				}
				else {
					hostsRanges.push(ipRangesObject[i][j]);
				}
			}
			// console.log("Exclude value: " + hostsRanges[i]);
		}
		// console.log("hostsRanges: " + hostsRanges);
		return hostsRanges;
	}
	this.excludeNetworkAndBroadcastAddressesForObjects = function(ipRangesObjectsArray) {
		for (var i = 0; i < Object.keys(ipRangesObjectsArray).length; i++) {
			ipRangesObjectsArray[i].rangeStart[3] = parseInt(ipRangesObjectsArray[i].rangeStart[3], 10) + 1;
			ipRangesObjectsArray[i].rangeEnd[3] = parseInt(ipRangesObjectsArray[i].rangeEnd[3], 10) - 1;
		}
		return ipRangesObjectsArray;
	}
	this.calculateIPFragment = function(value) {
		/*
		 * Calculate single IP fragment to binary which is
		 * further used in calculateIPAddress function.
		*/
		var binaryIP = [];
		var updatedValue = value;
		while (updatedValue >= 1) {
			binaryIP.push(updatedValue % 2);
			updatedValue = Math.floor(updatedValue / 2);
		}
		while (binaryIP.length < 8)
			binaryIP.push(0);
		binaryIP.reverse();
		this.ip[this.ipObjectLength()] = binaryIP;
		return binaryIP;
	}
	this.calculateIPAddress = function(ipInputsValues) {
		/*
		 * Concatenate IP fragments into whole IP address.
		*/
		var calculatedIP = [];
		var concatCalculatedIP = [];
		for (let i = 0; i < ipInputsValues.length; i++) {
			calculatedIP.push(this.calculateIPFragment(ipInputsValues[i]));
		}
		for (let i = 0; i < calculatedIP.length; i++) {
			concatCalculatedIP = concatCalculatedIP.concat(calculatedIP[i]);
		}
		return concatCalculatedIP;
	}
	this.calculateIPMask = function(maskInputValue) {
		/*
		 * Calculate mask written by user from /xx form to binary.
		*/
		var binaryMask = [];
		for (let i = 0; i < IP_AND_MASK_MAX_LENGTH; i++) {
			if (i < maskInputValue)
				binaryMask.push(1);
			else
				binaryMask.push(0);
		}
		return binaryMask;
	}
	this.calculateHosts = function(maskInputValue) {
		/*
		 * Calculate hosts number based on passed mask value.
		 * Function calculates 0's in mask by subtracting passed
		 * mask value from full mask value (32)
		*/
		return Math.pow(2, parseInt(IP_AND_MASK_MAX_LENGTH) - parseInt(maskInputValue)) - 2;
	}
	this.clear = function() {
		/*
		 * Clear whole data object and prepare it
		 * for new data set
		*/
		this.ip = {};
		this.mask = {};
		this.subnetsRangesObjectsArray = [];
	}
	this.showCalculatedRangesArray = function() {
		return this.subnetsRangesObjectsArray;
	}
	this.debugList = function() {
		var view = document.querySelector('.content');
		view.innerHTML = "";
		for (let i = 0; i < Object.keys(this.subnetsRangesObjectsArray).length / 2; i++) {
			console.log("[" + i + "]: ", this.subnetsRangesObjectsArray[i]);
			view.innerHTML += this.subnetsRangesObjectsArray[i].rangeStart + "/" + this.subnetsRangesObjectsArray[i].mask + " | " +
			this.subnetsRangesObjectsArray[i].rangeStart + " - " + this.subnetsRangesObjectsArray[i].rangeEnd + "</br>";
		}
	}
	this.ipObjectLength = function() {
		var length = Object.keys(this.ip).length;
		return Object.keys(this.ip).length;
	}
}

var Controller = function() {
	var ipcalc = new IPcalculator();
	this.takenSubnetsArray = [];

	this.getInputs = function(objectClass) {
		/*
		 * Get IP inputs from frontend.
		 * It is necessary to pass class name of desired objects
		 * to find them in document.
		*/
		var inputsCount = document.getElementsByClassName(objectClass);
		var inputs = [];
		for (let i = 0; i < inputsCount.length; i++) {
			inputs[i] = inputsCount[i].classList[1];
		}
		return inputs;
	}
	this.getInputsValues = function(objects) {
		/*
		 * This function is used to retrieve values from
		 * passed input objects. It uses querySelector with
		 * hardcoded . (dot) prefix, so it works only
		 * for inputs of passed class name.
		*/
		var inputsValues = [];
		for (let i = 0; i < objects.length; i++) {
			let currentInput = document.querySelector('.' + objects[i]);
			inputsValues.push(currentInput.value);
		}
		return inputsValues;
	}
	this.getIPAddress = function(objectClass) {
		var ipInputsValues = this.getInputsValues(this.getInputs(objectClass));
		return ipcalc.calculateIPAddress(ipInputsValues);
	}
	this.getMask = function(objectID) {

	}
	this.getRange = function(objectClass, maskInputID) {
		var ipInputsValues = this.getInputsValues(this.getInputs(objectClass));
		var maskInputValue = document.getElementById(maskInputID).value;
		let debug = ipcalc.range(ipInputsValues, maskInputValue);
		console.log("Range: ", debug.slice(0,4).join(".") + " - " + debug.slice(4,8).join("."));
		return ipcalc.range(ipInputsValues, maskInputValue);
	}
	this.getSubnetsRanges = function(objectClass, maskInputID, maskLimitInputID) {
		var ipInputsValues = this.getInputsValues(this.getInputs(objectClass));
		var maskInputValue = document.getElementById(maskInputID).value;
		var maskLimitInputValue = document.getElementById(maskLimitInputID).value;
		if (maskInputValue < 0 || maskInputValue > 30)
			return ERROR_INCORRECT_MASK_INPUT_VALUE;
		else
			return ipcalc.calculateSubnetsRanges(ipInputsValues, maskInputValue, maskLimitInputValue);
	}
	this.getHosts = function(objectClass, maskInputID) {
		var ipInputsValues = this.getInputsValues(this.getInputs(objectClass));
		var maskInputValue = document.getElementById(maskInputID).value;
		if (maskInputValue < 0 || maskInputValue > 30)
			return ERROR_INCORRECT_MASK_INPUT_VALUE;
		else
			return ipcalc.calculateHosts(maskInputValue);
	}
	this.getHostsRanges = function(objectClass, maskInputID, maskLimitInputID) {
		var ipInputsValues = this.getInputsValues(this.getInputs(objectClass));
		var maskInputValue = document.getElementById(maskInputID).value;
		var maskLimitInputValue = document.getElementById(maskLimitInputID).value;
		var subnetsRanges = ipcalc.calculateSubnetsRanges(ipInputsValues, maskInputValue, maskLimitInputValue);
		// console.log("subnetsRanges before excluding addresses: " + subnetsRanges);
		// return ipcalc.excludeNetworkAndBroadcastAddresses(subnetsRanges);
		if (parseInt(maskInputValue) < 0 || parseInt(maskInputValue) > 30)
			return ERROR_INCORRECT_MASK_INPUT_VALUE;
		else if (parseInt(maskLimitInputValue) <= parseInt(maskInputValue))
			return ERROR_INCORRECT_MASK_LIMIT_INPUT_VALUE;
		else
			return ipcalc.excludeNetworkAndBroadcastAddressesForObjects(subnetsRanges);
	}
	this.cleanRangesArray = function() {
		ipcalc.clear();
		this.takenSubnetsArray = [];
	}
	this.getSubnetsArray = function() {
		ipcalc.showCalculatedRangesArray();
	}
	this.getRecalculatedSubnets = function() {
		return ipcalc.recalculateSubnetsAfterExcludingRanges(this.takenSubnetsArray);
	}
	this.debug = function() {
		// ipcalc.debugList();
		ipcalc.showCalculatedRangesArray();
		ipcalc.clear();
	}
}
